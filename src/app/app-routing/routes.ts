import { Routes } from '@angular/router';

/* imported all the components for navigation */
import { MenuComponent } from '../menu/menu.component';
import { DishdetailComponent } from '../dishdetail/dishdetail.component';
import { HomeComponent } from '../home/home.component';
import { AboutComponent } from '../about/about.component';
import { ContactComponent } from '../contact/contact.component';


/* Routes is imported by app-routing.modules.ts */
export const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'menu', component: MenuComponent },
  { path: 'dishdetail/:id', component: DishdetailComponent },  
  { path: 'contactus', component: ContactComponent },
  { path: 'aboutus', component: AboutComponent },  
  { path: '', redirectTo: '/home', pathMatch: 'full' } ];
