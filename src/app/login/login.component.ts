import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user = {username: '', password: '', remember: false};

  // Reference to this LoginComponent => used to handle the submit dismission 
  // inside the onSubmit() below
  constructor(public dialogRef: MatDialogRef<LoginComponent>) { }

  ngOnInit() {
  }

  onSubmit() {
  	// visualize the data inserted by the user 
  	// into the console (F12 from the browser) 
    console.log('User: ', this.user);
    
    // we dismiss the dialog component
    this.dialogRef.close();  
  }

}
