import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router'; /* Routes is a const exported by routes.ts */

import { routes } from './routes';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)     /*.forRoot() takes as a parameter an array Route[] */
  ],
  exports: [ 
  	RouterModule 
  ],
  declarations: []
})
export class AppRoutingModule { }
